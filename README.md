# Projet Audit et diagnostique technique

Ce projet contient :

un site web [ce repertoire]       : contenant une page de connexion et une page d'accueil

une api [[api-service]](https://gitlab.com/juliencln1s1/audit-mds2/-/tree/main/api-service?ref_type=heads)             : faisant le lien avec la base de donnée

un programme java [[rainbowtables]](https://gitlab.com/juliencln1s1/audit-mds2/-/tree/main/rainbowtables?ref_type=heads
) : faisant une série de test sur des fichiers de log wireshark permettant de trouver le hash

# Installation

Le site web : http://audit.etiennemorvan.com/