# RainbowTables ReadMe

[[_TOC_]]

## Prérequis

- Java 19
- Soit un pc linux, soit un pc Windows 7 et + avec un shell Git Bash 

## Démarrage
Dans le dossier data, il y a 2 fichiers :
- `logs_wireshark.txt` : il contient les credentials sous forme de `user;login \n`
- `passwordsToTest.txt` : c'est la bibliothèque des mots de passes rainbowTables, elle contient les mots de passes 
connus en clair sous la forme `motdepasse \n`

Ouvrir un terminal, se rendre dans le dossier avec la commande `cd`, et lancer le fichier run.sh via la commande :
```bash
./run.sh
```
Celui-ci va se lancer en arrière-plan et créer un fichier `logs/nohup.log`.

Enfin, un fichier `data/output.txt` se crée, et il contient tous les logins avec mots de passe corrects, leur hash
et la méthode de cryptage utilisée.