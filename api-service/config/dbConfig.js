const mysql = require('mysql');

const pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'hyuz1950_auditmds2',
    connectTimeout: 10000
});

// Vérification de la connexion à la base de données
pool.getConnection((err, connection) => {
    if (err) {
        console.error("Impossible de se connecter à la base de données: ", err);
        process.exit(1); // Arrêt du serveur en cas d'échec de connexion
    } else {
        console.log("Connecté à la base de données avec succès");
        connection.release(); // Libération de la connexion
    }
});

module.exports = pool;
