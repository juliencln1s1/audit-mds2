const pool = require('../config/dbConfig');

exports.verifyUserPost = (req, res) => {
    const { username, password } = req.body;
    if (!req.body || Object.keys(req.body).length === 0) {
        return res.status(400).send({ message: 'Le corps de la requête est vide ou mal formé' });
    }
    pool.query('SELECT 1 FROM users WHERE username = ? && password_hash = ?', [username,password], async (error, results) => {
        if (error) {
            return res.status(500).json({ message: "Erreur interne du serveur " });
        }

        if (results.length === 0) {
            return res.status(401).json({ message: "Utilisateur non trouvé", user_found:false });
        }

        const user = results[0];

        if (user != null) {
            res.json({ message: "Utilisateur vérifié avec succès" , user_found: true});
        }
    });
};

exports.verifyUserGet = (req, res) => {
    const { username, password } = req.query;
    if (!req.query || Object.keys(req.query).length === 0) {
        return res.status(400).send({ message: 'Le corps de la requête est vide ou mal formé' });
    }
    pool.query('SELECT 1 FROM users WHERE username = ? && password_hash = ?', [username,password], async (error, results) => {
        if (error) {
            return res.status(500).json({ message: "Erreur interne du serveur " });
        }

        if (results.length === 0) {
            return res.status(401).json({ message: "Utilisateur non trouvé", user_found:false });
        }

        const user = results[0];

        if (user != null) {
            res.json({ message: "Utilisateur vérifié avec succès" , user_found: true});
        }
    });
};
