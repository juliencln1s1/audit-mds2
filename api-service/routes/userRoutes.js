const express = require('express');
const router = express.Router();
const { verifyUserPost, verifyUserGet } = require('../controllers/userController');

router.post('/verify-user', verifyUserPost);
router.get('/verify-user', verifyUserGet);

module.exports = router;
