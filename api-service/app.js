const express = require('express');
const cors = require('cors');
const userRoutes = require('./routes/userRoutes');
const { errorHandler } = require('./helpers/errorHandler');

const app = express();
app.use(express.json());

app.use(cors());

app.use('/', userRoutes);

app.use(errorHandler);

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Serveur lancé sur le port ${PORT}`);
});
