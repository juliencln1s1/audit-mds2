document.getElementById("loginForm").addEventListener("submit", function (event) {
    event.preventDefault();
    verifyUser();
});

function verifyUser() {
    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;
    var hashType = document.getElementById("hashType").value;

    var hashedPassword = CryptoJS[hashType](password).toString();

    var xhr = new XMLHttpRequest();
    xhr.open("GET", "http://audit.etiennemorvan.com/verify-user?username=" + username + "&password=" + hashedPassword, true);
    xhr.onreadystatechange = function () {
        console.log("response: ");
        if (this.readyState == 4) {
            if (this.status == 200 || this.status == 401) {
                var response = JSON.parse(this.responseText);
                handleResponse(response);
            }
        }
    };
    xhr.send();
}

function handleResponse(response) {

    if (response.user_found) {
        localStorage.setItem('username', document.getElementById("username").value);
        window.location.href = './pages/hello.html';
    } else {
        document.getElementById("errorMessage").textContent = "Utilisateur non trouvé";
        // Ajouter ou modifier l'image
        var img = document.getElementById("errorImage");
        if (!img) {
            img = document.createElement("img");
            img.id = "errorImage";
            document.body.appendChild(img); // Ajoute l'image au body, modifiez selon vos besoins
        }
        img.src = "/assets/img/500ko.jpg";
        img.style.display = "block";
        errorImage.style.position = 'absolute';
    }
}