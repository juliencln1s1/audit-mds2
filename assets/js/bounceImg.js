let horizon = 0; // 0 pour gauche, 1 pour droite
let vertical = 0; // 0 pour haut, 1 pour bas
let cube = document.querySelector('#errorImage');
let body = document.querySelector('body');

cube.style.top = (body.offsetHeight / 2) - 10 + "px";
cube.style.left = (body.offsetWidth / 2) - 10 + "px";

window.addEventListener("click", rebond);

function rebond() {
    window.removeEventListener("click", rebond);
    setInterval(() => {
        mouvement();
        let cubeTop = parseInt(cube.style.top.split("p")[0]);
        let cubeLeft = parseInt(cube.style.left.split("p")[0]);

        // Inverser la direction si l'image atteint un bord
        if (cubeTop <= 0 || cubeTop >= body.offsetHeight - cube.offsetHeight) {
            vertical = 1 - vertical;
        }
        if (cubeLeft <= 0 || cubeLeft >= body.offsetWidth - cube.offsetWidth) {
            horizon = 1 - horizon;
        }
    }, 50);
}

function mouvement() {
    let change = 10; // Déplacement en pixels
    let cubeTop = parseInt(cube.style.top.split("p")[0]);
    let cubeLeft = parseInt(cube.style.left.split("p")[0]);

    cube.style.top = (cubeTop + (vertical ? change : -change)) + "px";
    cube.style.left = (cubeLeft + (horizon ? change : -change)) + "px";
}
